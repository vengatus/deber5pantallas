//
//  MoodCell.swift
//  Deber5Pantallas
//
//  Created by Juan Erazo on 6/30/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class MoodCell: UITableViewCell , UITableViewDataSource, UITableViewDelegate {
    
    
    var n:Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell =  tableView.dequeueReusableCell(withIdentifier: "moodTitleCell", for: indexPath) as! MoodTitleTableViewCell
            cell.titleLabel.text = moods[self.n]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "sliderCell", for: indexPath) as! SliderTableViewCell
            cell.numberLabel.text = "5"
            return cell
        }
    }

}

//
//  textTableViewCell.swift
//  Deber5Pantallas
//
//  Created by Juan Erazo on 7/3/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class textTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.endEditing(true)
    }

}

//
//  SliderTableViewCell.swift
//  Deber5Pantallas
//
//  Created by Juan Erazo on 6/30/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class SliderTableViewCell: UITableViewCell {
    @IBOutlet weak var numberLabel: UILabel!

    @IBAction func moodChanged(_ sender: UISlider) {
        numberLabel.text = String(Int(sender.value * 10))
    }
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)
        // Configure the view for the selected state
    }

}

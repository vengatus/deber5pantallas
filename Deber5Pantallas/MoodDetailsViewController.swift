//
//  MoodDetailsViewController.swift
//  Deber5Pantallas
//
//  Created by Juan Erazo on 7/1/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit


class MoodDetailsViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var detailsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailsTableView.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "circleSliderCell", for: indexPath) as! CircularSliderTableViewCell
            cell.circularSlider.value = 5
            return cell
        } else {
            return tableView.dequeueReusableCell(withIdentifier: "textCell", for: indexPath)
        }
        
    }
    
    
    @IBAction func backPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    

    

}

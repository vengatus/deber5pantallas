//
//  ViewController.swift
//  Deber5Pantallas
//
//  Created by Juan Erazo on 6/28/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var todayTitle: UINavigationItem!
    @IBOutlet weak var moodsTableView: UITableView!
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return moods.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
  
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.groupTableViewBackground
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moodCell", for: indexPath) as! MoodCell
        cell.n = indexPath.section
        cell.layer.cornerRadius = 4
        return cell
    }
        
  	
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.moodsTableView.tableFooterView	 = UIView()
        
        let suffixes = ["st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                        "th", "th", "th", "th", "th", "th", "th", "th", "th", "th",
                        "th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th",
                        "th", "st"]
        let months = ["January" , "February" , "March" , "April", "May",
                      "June", "July", "August", "September", "October",
                      "November", "December"]
        
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        todayTitle.title = "Today, \(day)\(suffixes[day-1]) \(months[month-1])"
    }
    
    

}


//
//  CircularSliderTableViewCell.swift
//  Deber5Pantallas
//
//  Created by Juan Erazo on 7/2/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit
import CircularSlider

class CircularSliderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var moodLabel: UILabel!
    @IBOutlet weak var circularSlider: CircularSlider!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(false, animated: false)

        // Configure the view for the selected state
    }

}

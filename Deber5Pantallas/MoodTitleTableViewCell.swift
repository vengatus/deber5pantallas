//
//  MoodTitleTableViewCell.swift
//  Deber5Pantallas
//
//  Created by Juan Erazo on 7/1/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit

class MoodTitleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  CalendarViewController.swift
//  Deber5Pantallas
//
//  Created by Juan Erazo on 7/3/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CalendarViewController: UIViewController,JTAppleCalendarViewDelegate, JTAppleCalendarViewDataSource {
    let formatter  = DateFormatter()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        
        
        let parameters = ConfigurationParameters(startDate: formatter.date(from: "2017 01 01")!, endDate: formatter.date(from: "2017 12 31")!)
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "customCell", for: indexPath) as! CustomCell
        cell.label.text = cellState.text
        return cell
    }

    @IBAction func menuPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

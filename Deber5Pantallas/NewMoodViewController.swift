//
//  NewMoodViewController.swift
//  Deber5Pantallas
//
//  Created by Juan Erazo on 7/3/17.
//  Copyright © 2017 Juan Erazo. All rights reserved.
//

import UIKit



class NewMoodViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet var newMoodTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.newMoodTableView.tableFooterView = UIView()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return tableView.dequeueReusableCell(withIdentifier: "carouselCell", for: indexPath)
            } else {
                return tableView.dequeueReusableCell(withIdentifier: "sliderNewCell", for: indexPath)
            }
        
        } else {
            if indexPath.row == 0 {
                return tableView.dequeueReusableCell(withIdentifier: "textCell", for: indexPath)
                
            }
            let cell = UITableViewCell()
            cell.backgroundColor = UIColor.groupTableViewBackground
            return cell
        }
    }
   
    @IBAction func cancelPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func savePressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
